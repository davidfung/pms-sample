from django.db import models

class Permission(models.Model):
    class Meta:
        permissions = (
            ("export", "Can export data table"),
            ("backup", "Can download database"),
        )
