from django.contrib.auth.decorators import login_required
from django.views import generic

from utils.sysinfo import get_sysinfo

class About(generic.TemplateView):
    template_name = 'about.html'

    def get_context_data(self, **kwargs):
        info = get_sysinfo()
        context = super().get_context_data(**kwargs)
        context['info'] = info
        return context

class Dashboard(generic.TemplateView):
    template_name = 'index.html'

about = login_required(About.as_view())
dashboard = login_required(Dashboard.as_view())

