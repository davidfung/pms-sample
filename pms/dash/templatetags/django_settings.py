from django import template
from django.conf import settings

register = template.Library()

@register.simple_tag
def django_settings(name):
    '''Usage in template:
    {% load staticfiles django_settings %}
    <h1>{% django_settings "STATIC_ROOT" %}</h1>
    <h1>{% django_settings "INSTALLED_APPS" %}</h1>
    '''
    return str(settings.__getattr__(name))
