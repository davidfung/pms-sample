from django.apps import AppConfig

class DashConfig(AppConfig):
    name = 'dash'
    verbose_name = "System Dashboard"

    def ready(self):
        import dash.signals # register the signals
