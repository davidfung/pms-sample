import os
import sys

from django.test import TestCase

class UtilsFilenameTestCase(TestCase):
    
    def test_add_suffix(self):
        from utils.filename import add_suffix
        file1 = '/path/filename.ext'
        file2 = '/path/filename-new.ext'
        suffix = '-new'
        self.assertEqual(add_suffix(file1, suffix), file2)

    def test_add_suffix_ts(self):
        from utils.filename import add_suffix_ts
        file1 = '/path/filename.ext'
        file2 = '/path/filename-yyyymmdd-hhmmss.ext'
        self.assertEqual(len(add_suffix_ts(file1)), len(file2))
        pass

    def test_sysinfo(self):
        from utils.sysinfo import get_sysinfo
        info = get_sysinfo()
        self.assertTrue('PMS' in info)
        self.assertTrue('Django' in info)
        self.assertTrue('Python' in info)
