import os.path

from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse

from pms import settings
from utils.filename import add_suffix_ts

@permission_required('permission.backup')
def download(request):
    """
    A request handler to download the sqlite3 db file.
    """
    db_file = settings.DATABASES['default']['NAME']
    export_filename = add_suffix_ts(os.path.split(db_file)[1])
    with open(db_file, "rb") as f:
        buf = f.read()
    response = HttpResponse(content_type='application/x-sqlite3')
    response['Content-Disposition'] = 'attachment; filename="%s"' % export_filename
    response.write(buf)
    return response
