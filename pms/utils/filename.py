from datetime import datetime
import os.path

def add_suffix(filename, suffix):
    ''' 
    Add a suffix to a filename perserving the path and extension.
    Examples:
        add_suffix('/path/file.ext', '-new') returns '/path/file-new.ext'
    '''
    root, ext = os.path.splitext(filename)
    return root + suffix + ext

def add_suffix_ts(filename):
    '''
    Add a timestamp suffix to a filename preserving the path and extension.
    Examples:
        add_suffix_rs('/path/file.ext') returns '/path/file-yyyymmdd-hhnnss.ext'
    '''
    suffix = datetime.strftime(datetime.now(), '-%Y%m%d-%H%M%S')
    return add_suffix(filename, suffix)

