import pms.version
import django
import platform
from collections import OrderedDict

def get_sysinfo():
    dict = OrderedDict()
    dict['PMS'] = pms.version.VERSION
    dict['Django'] = django.get_version()
    dict['Python'] = platform.python_version()
    return dict
