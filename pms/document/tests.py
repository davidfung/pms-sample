import datetime

from django.core.urlresolvers import reverse
from django.test import TestCase

from client.models import Client
from document.forms import SearchForm
from document.models import Document, DocumentLine

class DocumentTestCase(TestCase):
    def setUp(self):
        client = Client.objects.create(company='ACME Test')
        client.save()
        doc = Document(doc_no=1001, client=client)
        doc.save()
        for _ in range(2):
            DocumentLine(document=doc).save()

    def test_document(self):
        doc = Document.objects.get(doc_no=1001)
        self.assertTrue(isinstance(doc, Document))
        self.assertTrue(2 == len(doc.documentline_set.all()))

    def test_resolvers(self):
        self.assertTrue('/document/', reverse('document-search'))
        self.assertTrue('/document/add', reverse('document-add'))
        self.assertTrue('/document/export', reverse('document-export'))
        self.assertTrue('/document/export/all', reverse('document-export-all'))
        self.assertTrue('/document/1001', reverse('document-detail', kwargs={'doc_no':1001}))
        self.assertTrue('/document/1001/edit', reverse('document-edit', kwargs={'doc_no':1001}))
        self.assertTrue('/document/1001/copy', reverse('document-copy', kwargs={'doc_no':1001}))
        self.assertTrue('/document/1001/delete', reverse('document-delete', kwargs={'doc_no':1001}))
        self.assertTrue('/document/1001/print', reverse('document-print', kwargs={'doc_no':1001}))

    def test_copy(self):
        doc1 = Document.objects.get(doc_no=1001)
        doc2 = doc1.copy()
        self.assertTrue(doc2.doc_no == 1002)
        self.assertTrue(doc1.company, doc2.company)
        self.assertTrue(len(doc1.documentline_set.all()) == 
                        len(doc2.documentline_set.all()) )

    def test_form(self):
        data = {
            'date_from': datetime.date(datetime.date.today().year, 1, 1),
            'date_to': datetime.date.today(),
            'paid': '',
        }
        form = SearchForm(data)
        self.assertTrue(form.is_valid())
