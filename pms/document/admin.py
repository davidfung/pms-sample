from django.contrib import admin
from document.models import Document, DocumentLine

class DocumentLineTabular(admin.TabularInline):
    model = DocumentLine

class DocumentAdmin(admin.ModelAdmin):
    list_display = ('doc_no', 'doc_date', 'client')
    inlines = [
        DocumentLineTabular,
    ]

admin.site.register(Document, DocumentAdmin)
admin.site.register(DocumentLine)

