from datetime import date
import logging

from django.core.urlresolvers import reverse_lazy
from django.db.models import (
    CharField, BooleanField, DateField, DecimalField, ForeignKey, 
    IntegerField, Model, TextField, Max, Sum,
)

from client.models import Client
from pms import settings


class Document(Model):

    STATUS_TYPE = (
        ('A', 'Active'),
        ('I', 'Inactive'),
    )
    DOC_TYPE = (
        ('I', 'Invoice'),
        ('Q', 'Quote'),
        ('R', 'Return'),
    )
    COMMENT_LINE_TAG = "C"

    client = ForeignKey(Client)
    doc_no = IntegerField(unique=True, editable=False)
    doc_type = CharField(max_length=1, choices=DOC_TYPE, default='I')
    doc_date = DateField(default=date.today)
    doc_note = TextField(blank=True, default='')
    subtotal = DecimalField(max_digits=7, decimal_places=2, default=0, editable=False)
    gst_apply = BooleanField(default=True)
    pst_apply = BooleanField(default=False)
    gst = DecimalField(max_digits=7, decimal_places=2, default=0, editable=False)
    pst = DecimalField(max_digits=7, decimal_places=2, default=0, editable=False)
    total = DecimalField(max_digits=7, decimal_places=2, default=0, editable=False)
    remark = TextField(blank=True, default='')
    paid_date = DateField(null=True, blank=True)
    paid_ref = CharField(max_length=32, null=True, blank=True)
    status = CharField(max_length=1, choices=STATUS_TYPE, default='A')
    company = CharField(blank=True, max_length=64, editable=False)
    address1 = CharField(blank=True, max_length=64, editable=False)
    address2 = CharField(blank=True, max_length=64, editable=False)
    city = CharField(blank=True, max_length=64, editable=False)
    prov = CharField(blank=True, max_length=64, editable=False)
    postcode = CharField(blank=True, max_length=12, editable=False)
    country = CharField(blank=True, max_length=32, editable=False)

    class Meta:
        ordering = ["doc_no"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        try:
            self.__original_client = self.client
        except Client.DoesNotExist:
            self.__original_client = None

    def __str__(self):
        return str(self.doc_no)

    def save(self, *args, **kwargs):
        # assign doc number to new document
        if not self.doc_no:
            self.doc_no = self.get_next_doc_no()
        # save first transaction date if applicable
        if not self.client.first_txn_date:
            self.client.first_txn_date = self.doc_date
            self.client.save()
        # update the last transaction date of the client
        if not self.client.last_txn_date or self.client.last_txn_date < self.doc_date:
            self.client.last_txn_date = self.doc_date
            self.client.save()
        # calculate the subtotal and taxes
        subtotal = self.documentline_set.aggregate(total=Sum('total'))['total']
        if subtotal:
            self.subtotal = subtotal
            self.gst = subtotal * settings.U_TAX_RATE_GST / 100 if self.gst_apply else 0
            self.pst = subtotal * settings.U_TAX_RATE_PST / 100 if self.pst_apply else 0
            self.total = subtotal + self.gst + self.pst
        # update company info if client has changed
        if self.id is None or self.client != self.__original_client:
            # TODO
            self.company = self.client.company
            self.address1 = self.client.address1
            self.address2 = self.client.address2
            self.city = self.client.city
            self.prov = self.client.prov
            self.postcode = self.client.postcode
            self.country = self.client.country
            self.__original_client = self.client
        return super(Document, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse_lazy('document-detail', kwargs={'doc_no': self.doc_no})

    def get_next_doc_no(self):
        last_doc_no = Document.objects.all().aggregate(Max('doc_no'))['doc_no__max']
        if isinstance(last_doc_no, int):
            next_doc_no = last_doc_no + 1
        else:
            next_doc_no = settings.U_INITIAL_DOC_NO
        return next_doc_no

    def copy(self):
        # Copy a document and all its lines, return the new document.
        # The new document has the client info of the original document
        # rather than the current info of the client.

        # create a new document
        doc = Document()
        doc.client = self.client
        doc.doc_no = self.get_next_doc_no()
        doc.doc_type = self.doc_type
        doc.doc_date = date.today()
        doc.doc_note = self.doc_note
        doc.subtotal = self.subtotal
        doc.gst_apply = self.gst_apply
        doc.pst_apply = self.pst_apply
        doc.gst = self.gst
        doc.pst = self.pst
        doc.total = self.total
        doc.remark = self.remark
        doc.status = self.status
        doc.company = self.company
        doc.address1 = self.address1
        doc.address2 = self.address2
        doc.city = self.city
        doc.prov = self.prov
        doc.postcode = self.postcode
        doc.country = self.country
        doc.save()        

        # recycle the document lines
        lines = self.documentline_set.all().order_by('seqno')
        for line in lines:
            line.id = None
            line.document = doc
            line.save()

        return doc

class DocumentLine(Model):
    document = ForeignKey(Document)
    seqno = IntegerField(default=9999)
    sku = CharField(max_length=16)
    desc = CharField(blank=True, max_length=80) 
    qty = DecimalField(max_digits=7, decimal_places=2, default=0) 
    unit_price = DecimalField(max_digits=7, decimal_places=2, default=0)
    total =DecimalField(max_digits=7, decimal_places=2, default=0, editable=False)

    class Meta:
        ordering = ['seqno']

    def save(self, *args, **kwargs):
        self.total = self.qty * self.unit_price
        return super(DocumentLine, self).save(*args, **kwargs)


def document_export(writer, docs=None):
    """
    writer is a csv writer object to export data to.
    docs is a queryset or a rawqueryset of the docs to export.
    """  
    logger = logging.getLogger(settings.U_LOGGER_ROOT + __name__)
    logger.info("document exported")
    writer.writerow([
        "client",
        "doc_no",
        "doc_type",
        "doc_date",
        "subtotal",
        "gst_apply",
        "pst_apply",
        "gst",
        "pst",
        "total",
        "paid_date",
        "paid_ref",
        "status",
    ])
    if docs:
        for doc in docs:
            writer.writerow([
                doc.client,
                doc.doc_no,
                doc.doc_type,
                doc.doc_date,
                doc.subtotal,
                doc.gst_apply,
                doc.pst_apply,
                doc.gst,
                doc.pst,
                doc.total,
                doc.paid_date,
                doc.paid_ref,
                doc.status,
            ])
