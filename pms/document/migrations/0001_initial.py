# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('doc_no', models.IntegerField(unique=True, editable=False)),
                ('doc_type', models.CharField(max_length=1, default='I', choices=[('I', 'Invoice'), ('Q', 'Quote'), ('R', 'Return')])),
                ('doc_date', models.DateField(default=datetime.datetime.today)),
                ('doc_note', models.TextField(default='', blank=True)),
                ('subtotal', models.DecimalField(decimal_places=2, default=0, max_digits=7, editable=False)),
                ('gst_apply', models.BooleanField(default=True)),
                ('pst_apply', models.BooleanField(default=False)),
                ('gst', models.DecimalField(decimal_places=2, default=0, max_digits=7, editable=False)),
                ('pst', models.DecimalField(decimal_places=2, default=0, max_digits=7, editable=False)),
                ('total', models.DecimalField(decimal_places=2, default=0, max_digits=7, editable=False)),
                ('remark', models.TextField(default='', blank=True)),
                ('paid', models.BooleanField(default=False)),
                ('paid_ref', models.CharField(max_length=32, blank=True, null=True)),
                ('status', models.CharField(max_length=1, default='A', choices=[('A', 'Active'), ('I', 'Inactive')])),
                ('client', models.ForeignKey(to='client.Client')),
            ],
            options={
                'ordering': ['doc_no'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DocumentLine',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('seqno', models.IntegerField(default=0)),
                ('sku', models.CharField(max_length=16)),
                ('desc', models.CharField(max_length=80, blank=True)),
                ('qty', models.DecimalField(decimal_places=0, default=0, max_digits=7)),
                ('unit_price', models.DecimalField(decimal_places=2, default=0, max_digits=7)),
                ('total', models.DecimalField(decimal_places=2, default=0, max_digits=7, editable=False)),
                ('document', models.ForeignKey(to='document.Document')),
            ],
            options={
                'ordering': ['seqno'],
            },
            bases=(models.Model,),
        ),
    ]
