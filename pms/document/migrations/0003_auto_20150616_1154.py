# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0002_auto_20150119_1935'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='paid_date',
            field=models.DateField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='document',
            name='doc_date',
            field=models.DateField(default=datetime.date.today),
        ),
        migrations.AlterField(
            model_name='documentline',
            name='seqno',
            field=models.IntegerField(default=9999),
        ),
    ]
