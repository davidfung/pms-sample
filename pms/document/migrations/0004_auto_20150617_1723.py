# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0003_auto_20150616_1154'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='document',
            name='paid',
        ),
        migrations.AlterField(
            model_name='document',
            name='paid_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
