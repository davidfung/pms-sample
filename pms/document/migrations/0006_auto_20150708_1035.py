# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0005_auto_20150707_1539'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='address1',
            field=models.CharField(max_length=64, blank=True, editable=False),
        ),
        migrations.AlterField(
            model_name='document',
            name='address2',
            field=models.CharField(max_length=64, blank=True, editable=False),
        ),
        migrations.AlterField(
            model_name='document',
            name='city',
            field=models.CharField(max_length=64, blank=True, editable=False),
        ),
        migrations.AlterField(
            model_name='document',
            name='company',
            field=models.CharField(max_length=64, blank=True, editable=False),
        ),
        migrations.AlterField(
            model_name='document',
            name='country',
            field=models.CharField(max_length=32, blank=True, editable=False),
        ),
        migrations.AlterField(
            model_name='document',
            name='postcode',
            field=models.CharField(max_length=12, blank=True, editable=False),
        ),
        migrations.AlterField(
            model_name='document',
            name='prov',
            field=models.CharField(max_length=64, blank=True, editable=False),
        ),
    ]
