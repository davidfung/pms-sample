import datetime

from django import db
from django import forms

from .models import Document, DocumentLine
from client.models import Client

def make_custom_datefield(f, **kwargs):
    formfield = f.formfield(**kwargs)
    if isinstance(f, db.models.DateField):
        formfield.widget.attrs.update({'class':'datePicker'})
    return formfield

class DocumentForm(forms.ModelForm):
    formfield_callback = make_custom_datefield
    class Meta:
        model = Document
        fields = [
            'client', 
            'doc_type', 
            'doc_date', 
            'doc_note', 
            'gst_apply', 
            'pst_apply', 
            'remark', 
            'paid_date',
            'paid_ref', 
            'status', 
            ]
        widgets = {
            'doc_note': forms.Textarea(attrs={'rows':4}),
            'remark': forms.Textarea(attrs={'rows':4}),
            }

LineFormSet = forms.models.inlineformset_factory(Document, DocumentLine, 
                  fields=['seqno', 'sku', 'desc', 'qty', 'unit_price'], 
                  widgets={
                    'seqno': forms.TextInput(attrs={'size':'10'}),
                    'sku': forms.TextInput(attrs={'size':'10'}),
                    'desc': forms.TextInput(attrs={'size':'50'}),
                    'qty': forms.TextInput(attrs={'size':'10'}),
                    'unit_price': forms.TextInput(attrs={'size':'10'})},
                  extra=2)

class SearchForm(forms.Form):
    client = forms.ModelChoiceField(
                required=False, 
                queryset = Client.objects.all(),
                )
    date_from = forms.DateField(
                required=False, 
                label="Date From", 
                initial=datetime.date(datetime.date.today().year,1,1), 
                widget=forms.TextInput(attrs={'class':'datePicker'}),
                )
    date_to = forms.DateField(
                required=False, 
                label="Date To", 
                initial=datetime.date.today, 
                widget=forms.TextInput(attrs={'class':'datePicker'}),
                )
    paid = forms.ChoiceField(
                required=False, 
                label="Paid", 
                choices=(('',''), ('y','Yes'),('n','No')),
                )
