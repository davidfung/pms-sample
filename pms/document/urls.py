from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from .views import (
    DocumentDetailView, DocumentCreateView, 
    DocumentUpdateView, DocumentDeleteView, DocumentPrintView, 
    document_copy_view, document_export_view, document_search_view, document_ar_view,
)

urlpatterns = patterns('',
    url(r'^$', login_required(document_search_view), name='document-search'),
    url(r'^add/$', login_required(DocumentCreateView.as_view()), name='document-add'),
    url(r'^export$', login_required(document_export_view), {'export_all': False}, name='document-export'),
    url(r'^export/all$', login_required(document_export_view), {'export_all': True}, name='document-export-all'),
    url(r'^(?P<doc_no>\d+)$', login_required(DocumentDetailView.as_view()), name='document-detail'),
    url(r'^(?P<doc_no>\d+)/edit$', login_required(DocumentUpdateView.as_view()), name='document-edit'),
    url(r'^(?P<doc_no>\d+)/copy$', login_required(document_copy_view), name='document-copy'),
    url(r'^(?P<doc_no>\d+)/delete$', login_required(DocumentDeleteView.as_view()), name='document-delete'),
    url(r'^(?P<doc_no>\d+)/print$', login_required(DocumentPrintView.as_view()), name='document-print'),
    url(r'^report/ar$', login_required(document_ar_view), name='document-report-ar'),
)
