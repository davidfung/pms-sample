import datetime
import pickle

from django.contrib.auth.decorators import permission_required
from django.core.urlresolvers import reverse_lazy
from django.db.models import Count, Sum
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView, View

from pms.settings import U_FORCE_ATTACHMENT
from report.documentreport import DocumentReport
from .models import Document, DocumentLine, document_export
from .forms import DocumentForm, LineFormSet, SearchForm

class DocumentDetailView(DetailView):
    model = Document
    template_name = 'document/detail.html'
    slug_field = 'doc_no'
    slug_url_kwarg = 'doc_no'

    def get_context_data(self, **kwargs):
        context = super(DocumentDetailView, self).get_context_data(**kwargs)
        doc = get_object_or_404(Document, doc_no=self.kwargs['doc_no'])
        context['lines'] = DocumentLine.objects.filter(document=doc)
        return context

class DocumentCreateView(CreateView):
    model = Document
    template_name = 'document/edit.html'
    slug_field = 'doc_no'
    slug_url_kwarg = 'doc_no'
    form_class = DocumentForm

    def get_success_url(self):
        return reverse_lazy('document-detail', args=(self.object.doc_no,))

    def form_valid(self, form):
        context = self.get_context_data()
        line_formset = context['line_formset']
        if line_formset.is_valid():
            # Save and commit the doc to get the doc id for saving the lines
            # and then save the doc again to update the subtotals. 
            self.object = form.save()
            line_formset.instance = self.object
            line_formset.save()
            form.save()
            # re-assign line seqnos
            if line_formset.has_changed():
                seqno = 10
                doc = Document.objects.get(doc_no=self.object.doc_no)
                for line in doc.documentline_set.order_by('seqno'):
                    line.seqno = seqno
                    line.save()
                    seqno += 10
            return super(DocumentCreateView, self).form_valid(form)
        else:
            return self.render_to_response(self.get_context_data(form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def get_context_data(self, **kwargs):
        context = super(DocumentCreateView, self).get_context_data(**kwargs)
        if self.request.method == 'POST':
            context['line_formset'] = LineFormSet(self.request.POST)
        elif self.request.method == 'GET':
            context['line_formset'] = LineFormSet()
        return context
           
class DocumentUpdateView(UpdateView):
    model = Document
    template_name = 'document/edit.html'
    slug_field = 'doc_no'
    slug_url_kwarg = 'doc_no'
    form_class = DocumentForm

    def get_success_url(self):
        return reverse_lazy('document-detail', args=(self.object.doc_no,))

    def form_valid(self, form):
        context = self.get_context_data()
        line_formset = context['line_formset']
        if line_formset.is_valid():
            # Save the doc to get the doc object for saving the lines
            # and then save the doc again to update the subtotals.
            # There is no need to commit the first time the doc is saved
            # because we don't need to generate a new doc id in update mode.
            self.object = form.save(commit=False)
            line_formset.instance = self.object
            line_formset.save()
            form.save()
            # re-assign line seqnos
            if line_formset.has_changed():
                seqno = 10
                doc = Document.objects.get(doc_no=self.object.doc_no)
                for line in doc.documentline_set.order_by('seqno'):
                    line.seqno = seqno
                    line.save()
                    seqno += 10
            return super(DocumentUpdateView, self).form_valid(form)
        else:
            return self.render_to_response(self.get_context_data(form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def get_context_data(self, **kwargs):
        context = super(DocumentUpdateView, self).get_context_data(**kwargs)
        if self.request.method == 'POST':
            context['line_formset'] = LineFormSet(self.request.POST, instance=self.object)
        elif self.request.method == 'GET':
            context['line_formset'] = LineFormSet(instance=self.object)
        return context
           
class DocumentDeleteView(DeleteView):
    template_name = 'document/delete.html'
    model = Document
    slug_field = 'doc_no'
    slug_url_kwarg = 'doc_no'
    success_url = reverse_lazy('document-search')

class DocumentPrintView(View):
    def get(self, request, doc_no):
        doc = get_object_or_404(Document, doc_no=doc_no)
        report = DocumentReport()
        pdf = report.run(doc=doc)
        response = HttpResponse(content_type='application/pdf')
        filename = "%s.pdf" % report.title
        if U_FORCE_ATTACHMENT:
            response['Content-Disposition'] = 'attachment; filename="%s"' % filename
        else:
            response['Content-Disposition'] = 'filename="%s"' % filename
        response.write(pdf)
        return response

@permission_required('permission.export')
def document_export_view(request, export_all):
    EXPORT_FILE = 'documents.csv'
    import csv
    from pms.settings import U_FORCE_ATTACHMENT

    if export_all:
        docs = Document.objects.order_by('doc_no')
    else:
        saved_query = request.session.get('document_search_query')
        if saved_query:
            query = pickle.loads(saved_query)
            docs = Document.objects.all()
            docs.query = query
        else:
            docs = None

    response = HttpResponse(content_type='text/csv')
    if U_FORCE_ATTACHMENT:
        response['Content-Disposition'] = 'attachment; filename="%s"' % EXPORT_FILE
    else:
        response['Content-Disposition'] = 'filename="%s"' % EXPORT_FILE
    writer = csv.writer(response)
    document_export(writer, docs)
    return response

def document_search_view(request):
    template_name = "document/search.html"
    object_list = None
    count = 0
    subtotal = 0 # before taxes
    grand_total = 0 # after taxes

    if request.method == 'POST':
        form = SearchForm(request.POST)
    else:
        data = {
            'date_from': datetime.date(datetime.date.today().year, 1, 1),
            'date_to': datetime.date.today(),
        }
        form = SearchForm(data)

    if form.is_valid():

        # retrieve documents based on selection criteria
        # and pickle the query in the session for export use
        client = form.cleaned_data['client']
        date_from = form.cleaned_data['date_from']
        date_to = form.cleaned_data['date_to']
        paid = form.cleaned_data['paid']
        kw = {}
        if client: kw['client'] = client
        if date_from: kw['doc_date__gte'] = date_from
        if date_to: kw['doc_date__lte'] = date_to
        #import pdb; pdb.set_trace()
        if paid == 'y':
            kw['paid_date__isnull'] = False
        elif paid == 'n':
            kw['paid_date__isnull'] = True
        object_list = Document.objects.filter(**kw)
        if object_list:
            request.session['document_search_query'] = pickle.dumps(Document.objects.filter(**kw).query)
        else:
            request.session['document_search_query'] = None

        # calculate the summary information 
        result = Document.objects.filter(**kw).aggregate(
            count=Count('id'),
            subtotal=Sum('subtotal'),
            grand_total=Sum('total'),
            )
        count = result['count']
        subtotal = result['subtotal']
        grand_total = result['grand_total']

    return render(request, template_name, 
        { 'form': form, 'object_list':object_list,
          'count':count, 'subtotal':subtotal, 'grand_total':grand_total,})

def document_copy_view(request, doc_no):
    template_name = "document/copy.html"

    if request.method == 'POST':
        doc1 = get_object_or_404(Document, doc_no=doc_no) 
        doc2 = doc1.copy()
        response = redirect('document-detail', doc2.doc_no)
    elif request.method == 'GET':
        object = get_object_or_404(Document, doc_no=doc_no) 
        response = render(request, template_name, {'object': object,})
    else:
        response = redirect('document-search')
    return response

def document_ar_view(request):
    template_name = "document/ar.html"
    object_list = None
    count = 0
    grand_total = 0

    # retrieve outstanding invoices
    kw = {'paid_date': None, 'doc_type': 'I'}
    object_list = Document.objects.filter(**kw)

    # calculate the summary information 
    result = Document.objects.filter(**kw).aggregate(
        count=Count('id'), grand_total=Sum('total'))
    count = result['count']
    grand_total = result['grand_total']

    return render(request, template_name, 
        { 'object_list':object_list,
          'count':count, 'grand_total':grand_total,})

