from django.test import TestCase

from client.models import Client

class ClientTestCase(TestCase):
    def setUp(self):
        pass

    def test_strip(self):
        c = Client.objects.create()
        c.company =' AMG '
        c.address1 =' 123 Main Street '
        c.address2 ='  '
        c.city =' Knowhere '
        c.prov =' B.C. '
        c.postcode =' 123 ABC '
        c.country =' Canada '
        c.save()
        self.assertTrue(isinstance(c, Client))
        self.assertEqual(c.company, 'AMG')
        self.assertEqual(c.address1, '123 Main Street')
        self.assertEqual(c.address2, '')
        self.assertEqual(c.city, 'Knowhere')
        self.assertEqual(c.prov, 'B.C.')
        self.assertEqual(c.postcode, '123 ABC')
        self.assertEqual(c.country, "Canada")

