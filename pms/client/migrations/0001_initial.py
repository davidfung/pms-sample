# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('company', models.CharField(max_length=64, unique=True)),
                ('address1', models.CharField(max_length=64, blank=True)),
                ('address2', models.CharField(max_length=64, blank=True)),
                ('city', models.CharField(max_length=64, blank=True)),
                ('prov', models.CharField(max_length=64, blank=True)),
                ('postcode', models.CharField(max_length=12, blank=True)),
                ('country', models.CharField(max_length=32, blank=True)),
                ('status', models.CharField(max_length=1, default='A', choices=[('A', 'Active'), ('I', 'Inactive')])),
                ('last_txn_date', models.DateField(blank=True, null=True)),
            ],
            options={
                'ordering': ['company'],
            },
            bases=(models.Model,),
        ),
    ]
