from django.contrib import admin
from .models import Client

class ClientAdmin(admin.ModelAdmin):
    list_display = ('company', 'address1', 'city', 'prov', 'postcode', 'status')

admin.site.register(Client, ClientAdmin)


