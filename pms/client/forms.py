from django import forms

from client.models import Client

class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = [
            'company', 
            'address1', 
            'address2', 
            'city', 
            'prov', 
            'postcode', 
            'country', 
            'status',
            'remark', 
            ]
        widgets = {
            'remark': forms.Textarea(attrs={'rows':4}),
            }
