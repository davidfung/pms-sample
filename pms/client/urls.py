from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from .views import (
    ClientListView, ClientDetailView, ClientCreateView, ClientUpdateView, ClientDeleteView,
    client_export_view,
)

urlpatterns = patterns('',
    url(r'^$', login_required(ClientListView.as_view()), name='client-list'),
    url(r'^add/$', login_required(ClientCreateView.as_view()), name='client-add'),
    url(r'^(?P<pk>\d+)/$', login_required(ClientDetailView.as_view()), name='client-detail'),
    url(r'^(?P<pk>\d+)/edit$', login_required(ClientUpdateView.as_view()), name='client-edit'),
    url(r'^(?P<pk>\d+)/delete/$', login_required(ClientDeleteView.as_view()), name='client-delete'),
    url(r'^export$', login_required(client_export_view)),
)
