from django.contrib.auth.decorators import permission_required
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView

from .models import Client, client_export
from .forms import ClientForm

class ClientListView(ListView):
    model = Client
    template_name = 'client/list.html'

class ClientDetailView(DetailView):
    model = Client
    template_name = 'client/detail.html'

class ClientCreateView(CreateView):
    model = Client
    template_name = 'client/create.html'
    form_class = ClientForm

class ClientUpdateView(UpdateView):
    model = Client
    template_name = 'client/update.html'
    fields = ['company', 'address1', 'address2', 'city', 'prov', 'postcode', 'country', 'status', 'remark']

class ClientDeleteView(DeleteView):
    template_name = 'client/delete.html'
    model = Client
    success_url = reverse_lazy('client-list')

@permission_required('permission.export')
def client_export_view(request):
    EXPORT_FILE = 'clients.csv'
    import csv
    from pms.settings import U_FORCE_ATTACHMENT
    response = HttpResponse(content_type='text/csv')
    if U_FORCE_ATTACHMENT:
        response['Content-Disposition'] = 'attachment; filename="%s"' % EXPORT_FILE
    else:
        response['Content-Disposition'] = 'filename="%s"' % EXPORT_FILE
    writer = csv.writer(response)
    client_export(writer)
    return response
