import logging

from django.db import models
from django.core.urlresolvers import reverse_lazy

from pms import settings

class Client(models.Model):
    STATUS_TYPE = (
        ('A', 'Active'),
        ('I', 'Inactive'),
    )
    company = models.CharField(unique=True, max_length=64)
    address1 = models.CharField(blank=True, max_length=64)
    address2 = models.CharField(blank=True, max_length=64)
    city = models.CharField(blank=True, max_length=64)
    prov = models.CharField(blank=True, max_length=64)
    postcode = models.CharField(blank=True, max_length=12)
    country = models.CharField(blank=True, max_length=32)
    status = models.CharField(max_length=1, choices=STATUS_TYPE, default='A')
    first_txn_date = models.DateField(null=True, blank=True)
    last_txn_date = models.DateField(null=True, blank=True)
    remark = models.TextField(blank=True, default='')

    class Meta:
        ordering = ["company"]

    def __str__(self):
        return self.company

    def get_absolute_url(self):
        return reverse_lazy('client-detail', kwargs={'pk': self.pk})

    def save(self, *args, **kwargs):
        self.company = self.company.strip()
        self.address1 = self.address1.strip()
        self.address2 = self.address2.strip()
        self.city = self.city.strip()
        self.prov = self.prov.strip()
        self.postcode = self.postcode.strip()
        self.country = self.country.strip()
        super().save(*args, **kwargs)

def client_export(writer):
    """
    writer is a csv writer object to export data to.
    """
    logger = logging.getLogger(settings.U_LOGGER_ROOT + __name__)
    logger.info('client exported')
    writer.writerow([
        "Company",
        "Address1",
        "Address2",
        "City",
        "Prov",
        "Postcode",
        "Country",
        "Status",
        "first_txn_date",
        "Last_txn_date",
        "Remark",
    ])
    for client in Client.objects.order_by('company'):
        writer.writerow([
            client.company,
            client.address1,
            client.address2,
            client.city,
            client.prov,
            client.postcode,
            client.country,
            client.status,
            client.first_txn_date,
            client.last_txn_date,
            client.remark,
        ])

