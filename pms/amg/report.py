from io import BytesIO
import sys

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter as letter
from reportlab.lib.units import inch as inch

"""Example

from report.basereport import BaseReport

class CustomReport(BaseReport):
    def setup(self, *args, **kwargs):
        super(CustomReport, self).setup(*args, **kwargs)
        # process report parameters here
        self.pos1 = args[0]
        self.kw1 = kwargs['keyword']
        self.constant1 = "SOME CONSTANT"
        self.draw_grid()
        print(self.pos1, self.kw1, self.constant1)
        return self

report = CustomReport()
pdf = report.run("postional arg", keyword="keyword argument")

"""

Portrait = 1
Landscape = 2

class BaseReport(object):

    title = 'Report Title'
    page_size = letter
    page_orientation = Portrait
    font = 'Helvetica'
    fontbold = 'Helvetica-Bold'
    fontitalic = 'Helvetica-Oblique'
    fontbolditalic = 'Helvetica-BoldOblique'
    default_fontsize_large = 12
    default_fontsize = 10
    default_fontsize_small = 8
    linespacing_factor = 1.2
    lmargin = 1.0 * inch
    rmargin = 1.0 * inch

    def __init__(self):
        if self.page_orientation == Portrait:
            self.width, self.height = self.page_size
        else:
            self.width, self.height = self.page_size[-1::-1]
        self.fontsize = self.default_fontsize
        # initialize canvas
        self.buffer = BytesIO()
        self.canvas = canvas.Canvas(self.buffer, pagesize=(self.width, self.height))
        self.canvas.setFont(self.font, self.fontsize)
        self.canvas.setTitle(self.title)

    def run(self, *args, **kwargs):
        '''A shortcut to run the report.'''
        return self.setup(*args, **kwargs).build().finalize()

    def setup(self, *args, **kwargs):
        '''Process report parameters here.

        This method is meant to be overrided by subclass.
        The instance canvas is ready for use in this method.
        Return iteself (for chaining purpose).
        '''
        if 'user' in kwargs:
            self.canvas.setAuthor(kwargs['user'].get_username())
        return self

    def build(self):
        """Build the report content.

        This method is meant to be overrided by subclass.
        Return itself (for chaining purpose).
        """
        return self

    def finalize(self):
        """Generate the PDF. 

        Call finalize() when the construction of document is complete.
        Return the PDF content.
        """
        self.canvas.save()
        content = self.buffer.getvalue()
        self.buffer.close()
        return content

    def draw_grid(self, pagesize=letter, interval=0.5*inch):
        """Draw a reference grid on the page for layout purpose."""
        c = self.canvas
        ty = self.ty
        # horizontal lines
        c.setLineWidth(0.1)
        for v in range(0, int(self.height), int(interval)):
            c.line(0, ty(v), self.width, ty(v))
        for h in range(0, int(self.width), int(interval)):
            c.line(h, ty(0), h, ty(self.height))

    def get_current_func_name(self):
        return sys._getframe(1).f_code.co_name

    def translate(self, x, y):
        """Translate the origin."""
        self.translate_x = x
        self.translate_y = y
        self.canvas.translate(x, y)

    def translate_back(self):
        """Restore the origin."""
        self.canvas.translate(-(self.translate_x), -(self.translate_y))

    @property
    def linespacing(self):
        return self.fontsize * self.linespacing_factor

    def lu(self, b):
        """Decode the byte stream in latin-1 and re-encode it to utf-8
           so that reportlab will not choked.
        """
        return b.decode('latin-1').encode('utf-8')

    def ty(self, y):
        """Translate the Y axis from bottom-up to top-down."""
        return self.height - y

    def th(self, h):
        """Translate the height from bottom-up to top-down."""
        return -(h)

    def nextline(self, y, count=1, linespacing_factor=None):
        """Move the y coorindate down one logical line."""
        if linespacing_factor:
            offset = self.fontsize * count * linespacing_factor
        else:
            offset = self.fontsize * count * self.linespacing_factor
        return y + offset

    def selectFont(self, font, fontsize=None):
        if fontsize:
            self.fontsize = fontsize
        self.canvas.setFont(font, self.fontsize)

    # Methods emulating reportlab counterparts with y-axis reversal.

    def drawString(self, x, y, text):
        self.canvas.drawString(x, self.ty(y), text)

    def drawRightString(self, x, y, text):
        self.canvas.drawRightString(x, self.ty(y), text)

    def rect(self, x, y, w, h, stroke=1, fill=0):
        self.canvas.rect(x, self.ty(y), w, self.th(h), stroke, fill)

    def line(self, x1, y1, x2, y2):
        self.canvas.line(x1, self.ty(y1), x2, self.ty(y2))

    def lineto(self, x, y):
        self.canvas.lineto(x, self.ty(y))

    def moveto(self, x, y):
        self.canvas.moveto(x, self.ty(y))

