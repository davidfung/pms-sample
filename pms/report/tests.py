from django.test import TestCase

from client.models import Client
from document.models import Document, DocumentLine
from report.documentreport import DocumentReport

class DocumentReportTestCase(TestCase):
    def setUp(self):
        client = Client.objects.create(company='ACME Test')
        client.save()
        doc = Document(doc_no=1001, client=client)
        doc.save()
        for _ in range(2):
            DocumentLine(document=doc).save()

    def test_documentreport(self):
        doc = Document.objects.get(doc_no=1001)
        report = DocumentReport()
        pdf = report.run(doc=doc)
        self.assertEqual(b'%PDF', pdf[:4])
