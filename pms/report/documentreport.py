import collections
import os.path

from django.conf import settings

from reportlab.lib.units import inch

from amg.report import BaseReport, inch, letter, Portrait

from document.models import Document, DocumentLine

class DocumentReport(BaseReport):

    title = 'Document Report'
    default_fontsize = 12
    default_fontsize_small = 10
    linespacing_factor = 1.5

    gst_no = "GST Registration # 99999 9999 RT9999"
    footer1 = "All amounts are in Canadian dollars.  Invoices are due upon receipt."
    footer2 = "Please make cheque payable to AMG Computing.  We appreciate your business."

    def setup(self, *args, **kwargs):
        super(DocumentReport, self).setup(*args, **kwargs)
        self.doc = kwargs['doc']
        self.amgblue = (0,0,0.5)
        self.title = "%s-%s" % (self.doc.get_doc_type_display(), self.doc.doc_no)
        #self.draw_grid()
        return self

    def build(self):
        '''Build the document report according to its type, e.g. invoice.

        Parameters:
            doc = the Document object to print

        Returns:
            Self
        '''
        # build all parts of the report
        self._build_header()
        self._build_billing_info()
        self._build_document_info()
        self._build_details()
        self._build_summary()
        self._build_footer()
        return self

    def _build_header(self):
        c = self.canvas
        c.saveState()
        c.setFillColorRGB(*self.amgblue)
        c.setStrokeColorRGB(*self.amgblue)

        # company name
        self.selectFont(self.fontbold, 14)
        x = self.lmargin + self.fontsize
        y = 1 * inch - self.fontsize/4
        self.drawString(x, y, "AMG Computing")

        # company logo 
        self.rect(inch, y+self.fontsize/8, 0.125 * inch, -self.fontsize, fill=1)
        
        # company info
        self.selectFont(self.font, 8.5)
        x = self.width - self.rmargin
        y = 1.0 * inch
        y = self.nextline(y, -1)
        self.drawRightString(x, y, "306-200 Capilano Road, Port Moody, B.C. V3H 5N1")
        y = self.nextline(y)
        self.drawRightString(x, y, "604-603-8721 amgcomputing.com")

        # dividing line
        c.setLineWidth(.8)
        y += 5
        self.line(1.0 * inch, y, 7.5 * inch, y)

        c.restoreState()

        # GST Registration No
        x = self.lmargin
        y = 1.25 * inch
        self.selectFont(self.font, 10)
        self.drawString(x, y, self.gst_no)

        return self

    def _build_billing_info(self):
        self.selectFont(self.font, 10)
        x = self.lmargin
        y = 1.75 * inch + self.fontsize
        self.drawString(x, y, "Bill To:")
        self.selectFont(self.fontbold, 12)
        y = self.nextline(y)
        self.drawString(x, y, self.doc.company)
        if self.doc.address1:
            y = self.nextline(y)
            self.drawString(x, y, self.doc.address1)
        if self.doc.address2:
            y = self.nextline(y)
            self.drawString(x, y, self.doc.address2)
        city_prov = "%s %s" % (self.doc.city, self.doc.prov)
        if city_prov:
            y = self.nextline(y)
            self.drawString(x, y, city_prov)
        country_postcode = "%s %s" % (self.doc.country, self.doc.postcode)
        if country_postcode:
            y = self.nextline(y)
            self.drawString(x, y, country_postcode)
        return self

    def _build_document_info(self):
        self.selectFont(self.fontbolditalic, 24)
        x = 5.75 * inch
        y = 1.75 * inch
        self.drawRightString(self.width - self.rmargin, y, self.doc.get_doc_type_display())
        self.selectFont(self.fontbold, 12)
        y = y0 = self.nextline(y, 2)
        self.drawString(x, y, "Invoice # :")
        y = self.nextline(y)
        self.drawString(x, y, "Date :")
        self.selectFont(self.fontbolditalic, 12)
        x = self.width - self.rmargin
        y = y0
        self.drawRightString(x, y, str(self.doc.doc_no))
        y = self.nextline(y)
        self.drawRightString(x, y, self.doc.doc_date.strftime("%Y-%m-%d"))
        return self

    def _build_details(self):
        colstops = (self.lmargin, self.lmargin+4.75*inch, self.lmargin+5.625*inch, self.width-self.lmargin)
        colheads = ("Description", "Qty", "Price", "Amount")
        colfuncs = (self.drawString, self.drawRightString, self.drawRightString, self.drawRightString)

        # column headings
        self.selectFont(self.font, self.default_fontsize)
        self.canvas.setLineWidth(.5)
        padding = 3
        y = 3.5 * inch
        for x, caption, fn in zip(colstops, colheads, colfuncs):
            fn(x, y, caption)
        self.line(self.lmargin, y-self.fontsize-padding, self.width-self.rmargin, y-self.fontsize-padding)
        self.line(self.lmargin, y+padding, self.width-self.rmargin, y+padding)

        # item details
        y = y + 0.3 * inch
        for line in self.doc.documentline_set.all().order_by('seqno'):
            x = colstops[0]; self.drawString(x, y, line.desc)
            if not line.sku.startswith(self.doc.COMMENT_LINE_TAG):
                x = colstops[1]; self.drawRightString(x, y, "{:.2f}".format(line.qty))
                x = colstops[2]; self.drawRightString(x, y, "{:.2f}".format(line.unit_price))
                x = colstops[3]; self.drawRightString(x, y, "{:,.2f}".format(line.total))
            y = self.nextline(y)
        return self

    def _build_summary(self):
        colstops = (self.lmargin+5.625*inch, self.width-self.lmargin)
        
        # footer divider
        y = 9 * inch
        self.selectFont(self.font, self.default_fontsize)
        self.canvas.setLineWidth(.5)
        padding = 3
        self.line(self.lmargin, y-self.fontsize-padding, self.width-self.rmargin, y-self.fontsize-padding)

        # doc summary
        self.drawRightString(colstops[0], y, 'SUBTOTAL')
        self.drawRightString(colstops[1], y, "{:,.2f}".format(self.doc.subtotal))
        y = self.nextline(y)
        self.drawRightString(colstops[0], y, 'GST')
        self.drawRightString(colstops[1], y, "{:,.2f}".format(self.doc.gst))
        y = self.nextline(y)
        self.drawRightString(colstops[0], y, 'BCPST')
        self.drawRightString(colstops[1], y, "{:,.2f}".format(self.doc.pst))
        y = self.nextline(y)
        self.selectFont(self.fontbold)
        self.drawRightString(colstops[0], y, 'TOTAL')
        self.drawRightString(colstops[1], y, "{:,.2f}".format(self.doc.total))

        # doc note
        y = 9 * inch
        textobj = self.canvas.beginText()
        textobj.setTextOrigin(self.lmargin, self.ty(y))
        textobj.setFont(self.font, self.default_fontsize_small)
        textobj.textLines(self.doc.doc_note.splitlines())
        self.canvas.drawText(textobj)

        return self

    def _build_footer(self):
        self.selectFont(self.fontitalic, self.default_fontsize_small)
        y = 10.125 * inch
        self.drawString(self.lmargin, y, self.footer1)
        y = self.nextline(y, 1, 1.2)
        self.drawString(self.lmargin, y, self.footer2)
        return self

