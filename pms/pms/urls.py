from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from pms.version import VERSION
from utils import database

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'extra_context': {'version':VERSION}}, name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page':'dashboard'}, name='logout'),

    url(r'^$', 'dash.views.dashboard', name='dashboard'),
    url(r'^about$', 'dash.views.about', name='about'),

    url(r'^client/', include('client.urls')),
    url(r'^document/', include('document.urls')),
    url(r'^db/download', database.download, name='db_download'),
)
