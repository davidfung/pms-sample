"""
Django settings for pms project.

"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

U_WEBAPP_ID = 'PMS'
U_WEBAPP_NAME = 'Project Management System'

U_DEBUG = False
U_TEMPLATE_DEBUG = False
U_ALLOWED_HOSTS = []
U_FORCE_ATTACHMENT = False
U_INITIAL_DOC_NO = 2000
U_LOGFILE_NAME = r'/path/to/log.txt'
U_LOGFILE_SIZE = 1 * 1024 * 1024
U_LOGFILE_COUNT = 2
U_LOGGER_ROOT = 'pms.'
U_SECRET_KEY = 'your secret key'
U_TAX_RATE_GST = 5 # in percentage
U_TAX_RATE_PST = 7 # in percentage
U_PG_ENABLED = False
U_PG_NAME = 'xxx' # database name
U_PG_USER = 'xxx'
U_PG_PASS = 'xxx'

try:
    from .local_settings import *
except:
    from .prod_settings import *

if U_PG_ENABLED:
    DATABASES = {
       'default': {
            # postgresql
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': U_PG_NAME,
            'USER': U_PG_USER,
            'PASSWORD': U_PG_PASS,
            'HOST': 'localhost',
            'PORT': '',
       }
    }
else:
    DATABASES = {
       'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
       }
    }


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = U_SECRET_KEY

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = U_DEBUG

TEMPLATE_DEBUG = U_TEMPLATE_DEBUG

ALLOWED_HOSTS = U_ALLOWED_HOSTS


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'dash.apps.DashConfig',
    'client',
    'document',
    'permission',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'pms.urls'

WSGI_APPLICATION = 'pms.wsgi.application'


# Internationalization

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Vancouver'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

# Template Files

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS
TEMPLATE_CONTEXT_PROCESSORS += (
    'django.core.context_processors.request',
)

# Formatting

DATE_FORMAT = "Y-m-d"
USE_THOUSAND_SEPARATOR = True

# Logging

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'logfile': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': U_LOGFILE_NAME,
            'maxBytes': U_LOGFILE_SIZE,
            'backupCount': U_LOGFILE_COUNT,
            'formatter': 'standard',
        },
    },
    'loggers': {
        U_LOGGER_ROOT.rstrip('.'): {
            'handlers': ['logfile'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}

# Although less secured, in order to store the query object in the
# request session, have to use the pickle based session serializer
# instead of the JSON based counterpart.
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'
