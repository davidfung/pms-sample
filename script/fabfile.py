# usage: fab deploy
# For deploy updates to production.
# This script is meant to be run from the dev environment.

import datetime

from fabric.api import run, env
from fabric.context_managers import cd

env.hosts = ['user@example.com',]

def deploy():

    with cd('~/path/to/pms'):
        run('mv pms pms-%s' % datetime.datetime.now().strftime("%y%m%d%H%M%S"))
        ...

