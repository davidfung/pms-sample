PMS - Project Management System

Goals
  - rewrite VFP PMS in Django
  - for internal use
  - single user app

Technical
  - designed to run on Linux
  - developed in Django 1.6
  - target to Django 1.7 (when released)
  - test with SQLite3/Postgresql database backends
  - Private source repository in BitBucket
  - reportlab - require apt-get install python-dev
  - bootstrap 3.1.1 template
  - jQuery 1.11.0
  - jQuery-UI 1.10.4 (datepicker)
  - the project version is defined in pms/__init__.py

Progresql
 
  - sudo apt-get install libpq-dev python-dev
  - sudo apt-get install postgresql-9.3
  - sudo apt-get install postgresql-contrib
  - sudo apt-get install postgresql-server-dev-9.3
  - sudo su -l postgres
      - createdb pms
      - createuser -P <user>
      - psql
          - grant all privileges on database pms to <user>;
          - alter role <user> createdb;
          - \q
      - exit

First Run

  - python manage.py migrate
  - python manage.py createsuperuser
  - python manage.py runserver

Design

  - use Django user framework to manage authentication 
    and authorization.

Requirements

  - ability to set initial invoice number
  - require login (use django auth module)
  - permission control (use django auth module)

Menu Design

  PMS --> Dashboard

  Client --> Client Search Form

  Invoice --> Invoice Search Form

  Report
    +-- Sales Report
    +-- A/R Report

  Utility
    +-- Export Invoice
    +-- Export Client
    +-- Download Database

  Logout


Forms

  Client maintenance form
  
  Document maintenance form (parent-child)
  - a line with a sku starts with 'C' is a comment line
  - generate invoice in PDF format

Reports

  - sales report (by client/ALL, date range default to current year)
  - a/r report

Permission System

  - Based on Django Permission System
  - Manage permission in the Permisson model

Backup/Export

  - export invoice data to xls
  - export client to xls
  - backup database - generate sqlite restore script

Production

  - hosted in webfaction: Django 1.8.2 (mod_wsgi 4.4.11/Python 3.4)

Deployment

  Initial Deployment
  - <removed>
  
  Subsequent Updates
  - <removed>

End
